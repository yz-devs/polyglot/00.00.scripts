#!/bin/bash
#
#--

function Usage() {
  echo "Usage: $0 [ -a AUTHOR ] [ -s SUFFIX ] [ -t TITLE ]" 1>&2
  exit 1
}
function MakeReadme () {
  cat <<EOF > $READMEPATH
---
Title   : "$FULLTITLE"
Language: "$LANGUAGE"
Author  : "$AUTHOR"
Date    : "$TODAY"
Modified: "$TODAY"
---

# $FULLTITLE

## Notes *[ Language : $LANGUAGE ]$PROJECT*  
EOF
}

#--

AUTHOR=""
SUFFIX=""
TITLE=""
#
while getopts ":h?a:s:t:" opts
do
  case "${opts}" in
    h|\?) Usage;;
    #
    a) AUTHOR=${OPTARG};;
    s) SUFFIX=${OPTARG};;
    t) TITLE=${OPTARG};;
    *) Usage;;
    :) Usage;;
  esac
done
#
if [ -z $AUTHOR ] || [ -z $SUFFIX ] || [ -z $TITLE ];
then
  Usage
fi
#
FULLTITLE="$SUFFIX.$TITLE"
LANGUAGES=( "ada" "c" "cpp" "cs" "fs" "go" "haskell" "kotlin" "python" "rust" )
#LANGUAGES=( "ada" "c" "cpp" "cs" "elixir" "fs" "go" "haskell" "kotlin" "python" "rust" )
TODAY=`date +%Y.%m.%d`

#--

READMEPATH="$FULLTITLE/README.md"
LANGUAGE=${LANGUAGES[@]}
#
mkdir $FULLTITLE
MakeReadme

#--

for LANGUAGE in ${LANGUAGES[@]};
do

  PROJECT=""

  #--

  LANGUAGEPATH="$FULLTITLE/$LANGUAGE"
  READMEPATH="$LANGUAGEPATH/README.md"
  #
  mkdir $LANGUAGEPATH
  MakeReadme

  #--

  SUBFOLDERS=( "App" "TsT" )
  for S in ${SUBFOLDERS[@]};
  do
    READMEPATH="$LANGUAGEPATH/$S/README.md"
    PROJECT="[ Project: $S ]"
    #
    mkdir -p $LANGUAGEPATH/$S
    MakeReadme
  done

  #--
done
