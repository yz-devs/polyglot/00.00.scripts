---
Title   : "CreateLanguagesStructure"
Language: "bash"
Author  : "Yzarca"
Date    : "2023.10.15"
Modified: "2023.10.15"
---

# CreateLanguagesStructure  
## Notes *[ Language : bash ]*  

- Languages = `Ada,C,C++,C#,F#,Go.Haskell,Kotlin,Python,Rust`

### Use  

``` bash
./CreateLanguagesStructure.sh
```
>```
>Usage: ./CreateLanguagesStructure.sh [ -a AUTHOR ] [ -s SUFFIX ] [ -t TITLE ]
>```

``` bash
./CreateLanguagesStructure.sh -a Yzarca -s 00.01 -t SecondTestToCreateStructure
#
tree 00.01.SecondTestToCreateStructure/
```
>```
>00.01.SecondTestToCreateStructure/
>├── README.md
>├── ada
>│   ├── App
>│   │   └── README.md
>│   ├── README.md
>│   └── TsT
>│       └── README.md
>├── c
>│   ├── App
>│   │   └── README.md
>│   ├── README.md
>│   └── TsT
>│       └── README.md
>├── cpp
>│   ├── App
>│   │   └── README.md
>│   ├── README.md
>│   └── TsT
>│       └── README.md
>├── cs
>│   ├── App
>│   │   └── README.md
>│   ├── README.md
>│   └── TsT
>│       └── README.md
>├── fs
>│   ├── App
>│   │   └── README.md
>│   ├── README.md
>│   └── TsT
>│       └── README.md
>├── go
>│   ├── App
>│   │   └── README.md
>│   ├── README.md
>│   └── TsT
>│       └── README.md
>├── haskell
>│   ├── App
>│   │   └── README.md
>│   ├── README.md
>│   └── TsT
>│       └── README.md
>├── kotlin
>│   ├── App
>│   │   └── README.md
>│   ├── README.md
>│   └── TsT
>│       └── README.md
>├── python
>│   ├── App
>│   │   └── README.md
>│   ├── README.md
>│   └── TsT
>│       └── README.md
>└── rust
>    ├── App
>    │   └── README.md
>    ├── README.md
>    └── TsT
>        └── README.md
>
>30 directories, 31 files
>```

---

*Final STructure With Projects*

``` sh
tree -d -L 3
```
``` text
.
├── ada
├── c
│   ├── App
│   │   ├── bin
│   │   ├── lib
│   │   ├── obj
│   │   └── src
│   └── TsT
│       ├── bin
│       ├── lib
│       ├── obj
│       └── src
├── cpp
│   ├── App
│   │   ├── bin
│   │   ├── lib
│   │   ├── obj
│   │   └── src
│   └── TsT
│       ├── bin
│       ├── obj
│       └── src
├── cs
│   ├── App
│   │   ├── bin
│   │   ├── obj
│   │   └── src
│   └── TsT
│       ├── bin
│       ├── obj
│       └── src
├── fs
│   ├── App
│   │   ├── bin
│   │   ├── obj
│   │   └── src
│   └── TsT
│       ├── bin
│       ├── obj
│       └── src
├── go
│   ├── App
│   │   ├── bin
│   │   └── src
│   └── TsT
│       ├── bin
│       └── src
├── go_trymodimport
│   ├── App
│   │   └── note
│   ├── TsT
│   ├── greetings
│   └── hello
├── haskell
├── kotlin
├── python
└── rust

57 directories
```